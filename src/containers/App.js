import React, {PropTypes} from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as RecordsActions from '../actions/RecordsActions';

import '../../styles/index.scss';

import Layout from '../components/Layout';

class App extends React.Component {
  render() {
    const { actions, records } = this.props;
    return (
      <Layout
        actions={actions}
        records={records}
        />
    );
  }
}

/*
 * Пробрасываем state в дочерние компоненты
 */
function mapStateToProps(state) {
  return {
    records: state.records
  }
}

/*
 * Пробрасываем диспетчер всевозможных действий
 */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, RecordsActions), dispatch),
  }
}

/*
 * Коннектим "Умный компонент" к приложению
 */
export default connect(mapStateToProps, mapDispatchToProps)(App)

