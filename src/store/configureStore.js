/*
 * Конфигурируем store. Создаем
 */
import { createStore, applyMiddleware, compose } from 'redux';

// Подключаем все редьюсеры
import rootReducer from '../reducers';

// Подключаем возможность асинхронных actions
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
  const store = compose(
    applyMiddleware(thunk),
  )(createStore)(rootReducer);
  return store;
}
