/*
 * Подключаем все редьюсеры приложения
 */
import {combineReducers} from 'redux';

import { routerReducer } from 'react-router-redux';

// Редьюсер для записей
import records from './records';

export default combineReducers({
  records,
  routing: routerReducer
});
