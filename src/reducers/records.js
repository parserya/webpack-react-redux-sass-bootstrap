import * as types from '../constants/ActionTypes';

const initialState = {
  items: [],
  fetching: false
};

export default function records(state = initialState, action) {
  switch (action.type) {
    case types.GET_RECORDS_REQUEST:
      return {
        ...state,
        fetching: false
      };
    case types.GET_RECORDS_SUCCESS:
      return {
        ...state,
        fetching: true,
        items: action.payload.records
      };
    default:
      return state;
  }
}
