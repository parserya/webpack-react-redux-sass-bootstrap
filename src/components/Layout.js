import React from 'react';

class Layout extends React.Component {
  componentDidMount() {
    this.props.actions.getRecords();
  }

  render() {
    const { records } = this.props;
    return(
      <div>
        Hello, world
        <ul>
          {
            records.fetching ?
              records.items.map((item) =>
                <li>{item.title}</li>
              )
            : <li>Загрузка...</li>
          }
        </ul>
      </div>
    );
  }
}

export default Layout;
