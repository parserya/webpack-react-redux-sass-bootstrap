import * as types from '../constants/ActionTypes';

export const getRecords = (nodeId = null) => {
  return (dispatch) => {
    dispatch({
      type: types.GET_RECORDS_REQUEST
    });

    setTimeout(() => {
      dispatch({
        type: types.GET_RECORDS_SUCCESS,
        payload: {
          records: [
            {
              title: 'Первый пункт'
            },
            {
              title: 'Второй пункт'
            }
          ]
        }
      });
    }, 100);
  }
}
